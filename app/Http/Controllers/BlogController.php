<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Función para redireccionar a la sección blog
    public function index()
    {
        return view('blog.index');
    }

    //Función para redireccionar a la sección blog agrerar registro
    public function add()
    {
        return view('blog.add');
    }

    //Función para agregar un nuevo registro
    public function addBlog(Request $request)
    {
        //control de excepción
        try {
            //arreglo con información que será enviada para resgitrar por medio API
            $data = array(
                'titulo' => $request->titulo,
                'autor' => $request->autor,
                'contenido' => $request->contenido,
                'fecha' => $request->fecha,
            );
            
            //configuración para conexión y envio de parámetros
            $ch = curl_init();
            $optArray = array(
                    CURLOPT_URL => env('API_BLOG_ENV',null)."addBlog",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array('Content-type: text/plain'),
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch,$optArray);
            $result = curl_exec($ch);
            $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //retorno de respuesta de la API
            return json_decode($result);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //Función para obtener todos los registros por medio de la API
    public function getBlogs(Request $request)
    {
        //control de excepción
        try {
            //configuración para conexión
            $ch = curl_init();
            $optArray = array(
                    CURLOPT_URL => env('API_BLOG_ENV',null)."getBlogs",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_POSTFIELDS => 0,
                    CURLOPT_HTTPHEADER => array('Content-type: text/plain'),
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch,$optArray);
            $result = curl_exec($ch);
            $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //retorno de respuesta de la API
            return json_decode($result);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //
    public function searchBlogs(Request $request)
    {
        //control de excepción
        try {
            //Parámtro para realizar búsqueda de registros
            $data = array(
                'search' => $request->search,
            );
            //configuración para conexión y envio de parámetros
            $ch = curl_init();
            $optArray = array(
                    CURLOPT_URL => env('API_BLOG_ENV',null)."searchBlogs",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array('Content-type: text/plain'),
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch,$optArray);
            $result = curl_exec($ch);
            $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //retorno de respuesta de la API
            return json_decode($result);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function getBlog($idBlog=null)
    {
        //control de excepción
        try {
            //configuración para conexión
            $ch = curl_init();
            $optArray = array(
                    CURLOPT_URL => env('API_BLOG_ENV',null)."getBlog/".$idBlog,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_POSTFIELDS => 0,
                    CURLOPT_HTTPHEADER => array('Content-type: text/plain'),
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch,$optArray);
            $result = curl_exec($ch);
            $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //retorno de respuesta de la API
            return json_decode($result);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
