<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Layout styles -->
    @yield('css')
    <!-- End layout styles -->
    <style>
        .footer {
            margin-left: 0;
            width: 100%;
        }
        .footer {
            background: #f2edf3;
            color: color(dark);
            border-top: 1px solid #e7dee9;
            padding: 30px 1rem;
            transition: all 0.25s ease;
            -moz-transition: all 0.25s ease;
            -webkit-transition: all 0.25s ease;
            -ms-transition: all 0.25s ease;
            font-size: calc(0.875rem - 0.05rem);
            font-family: "ubuntu-regular", sans-serif;
        }
    </style>

  </head>
  <body>
    <div class="container-scroller">
      @include('includes.navbar')

      <div class="container-fluid page-body-wrapper">
        @include('includes.sidebar')
        <div class="main-panel">
            @yield('content')
            @include('includes.footer')
        </div>
      </div>
    </div>

    <!-- plugins:js -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- endinject -->
    @yield('scripts')
    
  </body>
</html>