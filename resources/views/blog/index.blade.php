@extends('layouts.blog.app')

@section('title','Blog')

@section('content')
<div class="content-wrapper m-3">
    <div class="page-header">
        <h3 class="page-title">Listado de blogs</h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <p class="card-description"> Registrar nuevo blog</p>
                    <div class="col-12 my-1">
                        <a class="button btn btn-primary" href="{{ route('add') }}">
                            Nuevo
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card m-1">
            <div class="card">
                <div class="card-body">
                    <p class="card-description"> Realiza búsqueda de blogs por título, contenido o autor</p>
                    <form id="searchForm" name="searchForm" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="search" class="form-control" id="search" name="search" placeholder="Escribe aquí">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-1">
                            <button type="submit" class="btn btn-primary" onclick="searchBlogs();">Buscar</button>
                        </div>
                    </form>
                    <div class="row mb-2">
                        <span class="row contentBlog"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalBlog" tabindex="-1" role="dialog" aria-labelledby="blogTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title blogTitulo"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row m-2">
                        <p class="blogContenido"></p>
                        <p><b><i class="blogAutor"></i>&nbsp;&nbsp;</b>
                        <small class="blogFecha"></small><br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section( 'scripts' )
    <script src="{{ asset('assets/lib/jquery.js') }}"></script>

    <script>

        $(function() {
            $('#searchForm').on('submit', function (event) {
                event.preventDefault();
            });
        });

        $(document).ready(function() {
            getBlogs();
        });

        function getBlogs() {
            Swal.fire({
                title: 'Cargando registros',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            });
            $.ajax({
                url: "/getBlogs",
                type: "GET",
                dataSrc: '',
                success: function(response) {
                    let contentBlog = '';
                    $('.contentBlog').val('');
                    $('.contentBlog').empty();
                    if(!jQuery.isEmptyObject(response.blogs))
                    {
                        let blogs = response.blogs;
                        contentBlog = buildContent(blogs);
                    }else{
                        contentBlog += '<div class="col-md-12 text-center">'+
                            '<p><strong>No hay blogs registrados</strong></p>'+ 
                        '</div>';
                    }
                    $('.contentBlog').append(contentBlog);
                }
            }).always(function(){
                Swal.close();
            });
        }

        function searchBlogs() {
            if( $('#search').val() == ""){
                getBlogs();
                return false;
            }

            Swal.fire({
                title: 'Búscando registros',
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    Swal.showLoading();
                }
            });

            let allData = {
                _token: $( 'meta[ name="csrf-token" ]' ).attr( 'content' ),
                search: $('#search').val()
            };

            $.ajax({
                url: "/searchBlogs",
                type: "POST",
                dataSrc: '',
                data: allData,
                success: function(response) {
                    $('.contentBlog').val('');
                    $('.contentBlog').empty();
                    let contentBlog = '';
                    if(!jQuery.isEmptyObject(response.blogs))
                    {
                        let blogs = response.blogs;
                        contentBlog = buildContent(blogs);
                    }else{
                        contentBlog += '<div class="col-md-12 text-center">'+
                            '<p><strong>No hay blogs registrados con esos parametros de búsqueda</strong></p>'+ 
                        '</div>';
                    }                    
                    $('.contentBlog').append(contentBlog);
                }
            }).always(function(){
                Swal.close();
            });
        }

        function buildContent(blogs){
            let contentBlog = '';
            blogs.forEach(function(blog) {
                contentBlog += '<div class="col-md-6">'+
                    '<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">'+
                        '<div class="col p-4 d-flex flex-column position-static">'+
                        '<strong class="d-inline-block mb-2 text-primary">'+blog.autor+'</strong>'+
                        '<h3 class="mb-0">'+blog.titulo+'</h3>'+
                        '<div class="mb-1 text-muted">'+blog.fecha_publicacion+'</div>'+
                        '<p class="card-text mb-auto">'+blog.contenido.substring(0, 69)+'...'+'</p>'+
                        '<a href="#" type="button" class="btn btn-primary stretched-link" data-toggle="modal" data-target="#modalBlog" onclick="showBlog('+blog.id+');">Ver</a>'+
                        '</div>'+
                    '</div>'+
                '</div>';
            });
            return contentBlog;
        }

        function showBlog(blog){
            $('#modalBlog').hide();
            $.ajax({
                url: "/getBlog/"+blog,
                type: "GET",
                dataSrc: '',
                async: false,
                success: function(response) {
                    if(!jQuery.isEmptyObject(response.blog))
                    {
                        let blog = response.blog;
                        $('.blogTitulo').html(blog.titulo);
                        $('.blogContenido').html(blog.contenido);
                        $('.blogAutor').html(blog.autor);
                        $('.blogFecha').html(blog.fecha_publicacion);
                        $('#modalBlog').show();
                    }
                }
            });
        }
    </script>
@endsection