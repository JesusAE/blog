@extends('layouts.blog.app')

@section('title','Alta de registros')

<style>
	.error{
        color: red;
        border: 0;
        margin-bottom: 3px;
        display: block;
        width: 100%;
    }
</style>

@section('content')
<div class="content-wrapper">
    <div class="m-2">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('blog') }}">Volver al listado</a></li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Nuevo</h4>
                    <p class="card-description"> Importación de usuarios mediante archivo</p>
                    <form id="addForm" name="addForm" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo">Título</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-multiple text-success"></i></span>
                                        </div>
                                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Ingresa un título">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="autor">Autor</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-multiple text-success"></i></span>
                                        </div>
                                        <input type="text" class="form-control" id="autor" name="autor" placeholder="Ingresa un autor">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fecha">Fecha</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-multiple text-success"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fecha" name="fecha">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="titulo">Contenido</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="mdi mdi-account-multiple text-success"></i></span>
                                        </div>
                                        <textarea class="form-control" id="contenido" name="contenido" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-3">
                            <button type="submit" class="btn btn-primary mb-2">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section( 'scripts' )
    <script src="{{ asset('assets/lib/jquery.js') }}"></script>
    <script src="{{ asset('assets/dist/jquery.validate.js') }}"></script>

    <script>
        $.validator.setDefaults({
            submitHandler: function() {
                add();
            }
        });

        $(document).ready(function() {
            $("#addForm").validate({
                rules:{
                    titulo:{
                        required: true,
                        minlength: 3,
                        maxlength: 200
                    },
                    autor:{
                        required: true,
                        minlength: 3,
                        maxlength: 200,
                    },
                    contenido:{
                        required: true,
                        minlength: 10,
                        maxlength: 200,
                    },
                    fecha:{
                        required: true,
                    }
                },
                errorClass: 'error',
                messages: {
                    titulo: {
                        required: "El campo Título es requerido",
                        minlength: "3 caracteres como  mínimo",
                        maxlength: "200 caracteres como máximo"
                    },
                    autor: {
                        required: "El campo Autor es requerido",
                        minlength: "3 caracteres como  mínimo",
                        maxlength: "200 caracteres como máximo"
                    },
                    contenido: {
                        required: "El campo Contenido es requerido",
                        minlength: "10 caracteres como  mínimo",
                        maxlength: "200 caracteres como máximo"
                    },
                    fecha: "El campo fecha es requerido"
                },
            });
        });

        function add()
        {
            let allData = {
                _token: $( 'meta[ name="csrf-token" ]' ).attr( 'content' ),
                titulo: $('#titulo').val(),
                autor: $('#autor').val(),
                contenido: $('#contenido').val(),
                fecha: $('#fecha').val()
            };

            $.ajax({
                url: "/addBlog",
                type: "POST",
                dataSrc: '',
                data: allData,
                async:false,
                success: function(response) {
                    if(response.response == 200){
                        let form = $('#addForm');
                        form[0].reset();
                        Swal.fire({
                            icon: 'success',
                            title: 'Exito',
                            text: response.message
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: '¡Error!',
                            text: response.message
                        })
                    }
                }
            });
        }
    </script>

@endsection