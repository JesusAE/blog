<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blog.index');
});

#inicio rutas Blog
Route::get('/blog','App\Http\Controllers\BlogController@index')->name('blog');
Route::get('/add', 'App\Http\Controllers\BlogController@add')->name('add');
Route::post('/addBlog', 'App\Http\Controllers\BlogController@addBlog');
Route::get('/getBlogs', 'App\Http\Controllers\BlogController@getBlogs');
Route::get('/getBlog/{idBlog}', 'App\Http\Controllers\BlogController@getBlog');
Route::post('/searchBlogs', 'App\Http\Controllers\BlogController@searchBlogs');
#final rutas Blog